// RGB2HSV.cpp : image ppm (RGB) to ppm (HSV)

#include <algorithm>
#include <stdio.h>
#include <algorithm>
#include "image_ppm.h"

using namespace std;

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageInRGB.ppm ImageOutHSV.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;

   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);

   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);

   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

   allocation_tableau(ImgOut, OCTET, nTaille3);

/*
   int r,g,b ;
   r,g,b = 0;

  for (int i=0; i < nTaille3; i+=3)
     {
        r = r / 255.0;
        g = g / 255.0;
        b = b / 255.0;
 
        double cmax = max(r, max(g, b)); // maximum of r, g, b
        double cmin = min(r, min(g, b)); // minimum of r, g, b
        double diff = cmax - cmin; // diff of cmax and cmin.
        int h = -1, s = -1;
         
        // if cmax and cmax are equal then h = 0
        if (cmax == cmin)
            h = 0;
 
        // if cmax equal r then compute h
        else if (cmax == r)
            h = (int)(60 * ((g - b) / diff) + 360) % 360;
 
        // if cmax equal g then compute h
        else if (cmax == g)
            h = (int)(60 * ((b - r) / diff) + 120) % 360;
 
        // if cmax equal b then compute h
        else if (cmax == b)
            h = (int)(60 * ((r - g) / diff) + 240) % 360;

        // if cmax equal zero
        if (cmax == 0)
            s = 0;
        else
            s = (diff / cmax) * 100;
 
        // compute v
        double v = cmax * 100;
        
        ImgOut[i] = h;
        ImgOut[i+1] = s;
        ImgOut[i+2] = v;
 
    }
*/

   float nR, nG, nB;

    float h,s,v;
    float rgbMin, rgbMax;

    h,s,v,rgbMin,rgbMax = 0;

    for (int i=0; i < nTaille3; i+=3)
     {
      nR = ImgIn[i]; //R
       nG = ImgIn[i+1]; //G
       nB = ImgIn[i+2]; //B

        rgbMin = nR < nG ? (nR < nB ? nR : nB) : (nG < nB ? nG : nB);
        rgbMax = nR > nG ? (nR > nB ? nR : nB) : (nG > nB ? nG : nB);

        v = rgbMax;
        if (v == 0)
        {
            h = 0;
            s = 0;
            ImgOut[i]=h;
            ImgOut[i+1]=s;
            ImgOut[i+2]=v;
        }

        s = 255 * long(rgbMax - rgbMin) / v;
        if (s == 0)
        {
            h = 0;
            ImgOut[i]=h;
            ImgOut[i+1]=s;
            ImgOut[i+2]=v;
        }

        if (rgbMax == nR)
            h = 0 + 43 * (nG - nB) / (rgbMax - rgbMin);
        else if (rgbMax == nG)
            h = 85 + 43 * (nB - nR) / (rgbMax - rgbMin);
        else
            h = 171 + 43 * (nR - nG) / (rgbMax - rgbMin);

        ImgOut[i]=h;
        ImgOut[i+1]=s;
        ImgOut[i+2]=v;
  }

   /*
  int nR, nG, nB;
  nR = 0;
  nG = 0;
  nB = 0;
  
   for (int i=0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i]; //R
       nG = ImgIn[i+1]; //G
       nB = ImgIn[i+2]; //B

       //printf("test 1\n");

       printf("nR = %d\n",nR);
       //printf("nG = %d\n",nG);
       //printf("nB = %d\n",nB);

       nR = nR/255;
       nG = nG/255;
       nB = nB/255;

       printf("nR = %d\n",nR);
       //printf("nG = %d\n",nG);
       //printf("nB = %d\n",nB);

       int maximum = max(max(nR,nG),nB);
       int minimum = min(min(nR,nG),nB);

      // printf("test 2\n");
       //calcul de H
       if(maximum==nR) 
       {
        ImgOut[i]= (nG-nB)/(maximum-minimum);
       }
       if(maximum==nG) 
       {
        ImgOut[i]= ((nB-nR)/(maximum-minimum))+2;
       }
       if(maximum==nB) 
       {
        ImgOut[i]= ((nR-nG)/(maximum-minimum))+4;
       }

       //printf("test 3 \n");
       ImgOut[i]=(ImgOut[i]+6)%6;
       ImgOut[i]=ImgOut[i] * 255; //H

       ImgOut[i+1]=maximum * 255; //S

       //printf("test 4 \n");

       if(ImgOut[i+1]==0) ImgOut[i+2]=0;
       else ImgOut[i+2]=(maximum-minimum)/ImgOut[i+1]; //V

       ImgOut[i+2] = ImgOut[i+2] * 255;

       printf("ImgOut[i] = %d\n",ImgOut[i]);
       //printf("ImgOut[i+1] = %d\n",ImgOut[i+1]);
       //printf("ImgOut[i+2] = %d\n",ImgOut[i+2]);

       //printf("test 5 \n");
     }
*/

 
   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
   return 1;
}

