// modifY.cpp : image Y.pgm to Y modif.pgm

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, Y, Cb, Cr,k;
  
  if (argc != 4) 
     {
       printf("Usage: Y.pgm int k ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2], "%d", &k);
   sscanf (argv[3],"%s",cNomImgEcrite) ;
   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);

   
   nTaille = nH * nW;
   
   allocation_tableau(ImgIn, OCTET, nTaille);
   
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   
   allocation_tableau(ImgOut, OCTET, nTaille);

   
   for(int i=0;i<nH;i++)
     for(int j=0;j<nW;j++)
       ImgOut[i*nW+j]=ImgIn[i*nW+j];

    for(int i=0;i<nH;i++)
     for(int j=0;j<nW;j++)
       {
	 if(ImgIn[i*nW+j]+k<0) ImgOut[i*nW+j]=0;
	 else if(ImgIn[i*nW+j]+k>255) ImgOut[i*nW+j]=255;
	 else ImgOut[i*nW+j]=ImgIn[i*nW+j]+k;
       }

  

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;
}
