// HSV2RGB.cpp : image ppm (HSV) to ppm (RGB)

#include <algorithm>
#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

using namespace std;

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageInHSV.ppm ImageOutRGB.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;

   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);

   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);

   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

   allocation_tableau(ImgOut, OCTET, nTaille3);
   
for (int i=0; i < nTaille3; i+=3)
     {
	    double r = 0, g = 0, b = 0;

	  if (ImgIn[i+1] == 0)
		  {
		    r = ImgIn[i+2];
		    g = ImgIn[i+2];
		    b = ImgIn[i+2];
		  }
	  else
	  {
	    int j;
	    double f, p, q, t;

	    if (ImgIn[i] == 360)
	      ImgIn[i] = 0;
	    else
	      ImgIn[i] = ImgIn[i] / 60;

	    j = (int)trunc(ImgIn[i]);
	    f = ImgIn[i] - j;

	    p = ImgIn[i+2] * (1.0 - ImgIn[i+1]);
	    q = ImgIn[i+2] * (1.0 - (ImgIn[i+1] * f));
	    t = ImgIn[i+2] * (1.0 - (ImgIn[i+1] * (1.0 - f)));

	    switch (i)
	    {
	    case 0:
	      r = ImgIn[i+2];
	      g = t;
	      b = p;
	      break;

	    case 1:
	      r = q;
	      g = ImgIn[i+2];
	      b = p;
	      break;

	    case 2:
	      r = p;
	      g = ImgIn[i+2];
	      b = t;
	      break;

	    case 3:
	      r = p;
	      g = q;
	      b = ImgIn[i+2];
	      break;

	    case 4:
	      r = t;
	      g = p;
	      b = ImgIn[i+2];
	      break;

	    default:
	      r = ImgIn[i+2];
	      g = p;
	      b = q;
	      break;
    }

  }
  ImgOut[i]= r * 255 ;
  ImgOut[i+1] = g * 255;
  ImgOut[i+2] = b * 255;
}
   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
   return 1;
}