// YCbCr.cpp : image YCbCr to ppm

#include <stdio.h>
#include "image_ppm.h"
/*
int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgLue2[250], cNomImgLue3[250], cNomImgEcrite[250];
  int nH, nW, nTaille, Y, Cb, Cr;
  
  if (argc != 5) 
     {
       printf("Usage: Y.pgm Cb.pgm Cr.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgLue2) ;
   sscanf (argv[3],"%s",cNomImgLue3);
   sscanf (argv[4],"%s",cNomImgEcrite) ;

   OCTET *ImgIn,*ImgIn2, *ImgIn3, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);

   
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   
   allocation_tableau(ImgIn, OCTET, nTaille3);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   allocation_tableau(ImgIn3, OCTET, nTaille);
   
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
   lire_image_pgm(cNomImgLue3, ImgIn3, nH * nW);
   
   allocation_tableau(ImgOut, OCTET, nTaille3);
   
   for (int i=0; i < nTaille3; i+=3)
     {
       Y = ImgIn[i/3];
       Cb = ImgIn2[i/3];
       Cr = ImgIn3[i/3];

       
       if(Y + 1.402*(Cr-128)<0) ImgOut[i]=0;
       else if(Y + 1.402*(Cr-128)>255) ImgOut[i]=255;
       else ImgOut[i]= Y + 1.402*(Cr-128); //R
       
       
       if(Y-0.34414*(Cb-128)-0.714414*(Cr-128)<0) ImgOut[i]=0;
       else if(Y-0.34414*(Cb-128)-0.714414*(Cr-128)>255) ImgOut[i]=255;
       else ImgOut[i+1]=Y-0.34414*(Cb-128)-0.714414*(Cr-128); //G
       
       
       if(Y+1.772*(Cb-128)<0) ImgOut[i]=0;
       else if(Y+1.772*(Cb-128)>255) ImgOut[i]=255;
       else ImgOut[i+2]=Y+1.772*(Cb-128); //B
       

      
      // erreur débordements
        
       //ImgOut[i]= Y + 1.402*(Cr-128);
      // ImgOut[i+1]=Y-0.34414*(Cb-128)-0.714414*(Cr-128);
      // ImgOut[i+2]=Y+1.772*(Cb-128);
       
     }

   
   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);free(ImgIn2);free(ImgIn3);
   return 1;
}
*/


#define MAX(X, Y) (((X) < (Y)) ? (Y) : (X))
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))


int main(int argc, char const *argv[])
{
	char cNomImgEcrite[250], cNomImgY[250], cNomImgCb[250], cNomImgCr[250];
	int nH, nW, nTaille;

	if (argc != 5) 
     {
       printf("Usage: Y.pgm Cb.pgm Cr.pgm ImageOut.ppm\n"); 
       exit (1) ;
     }

    sscanf (argv[4],"%s",cNomImgEcrite) ;
    sscanf (argv[1],"%s",cNomImgY);
    sscanf (argv[2],"%s",cNomImgCb);
    sscanf (argv[3],"%s",cNomImgCr);

    

    OCTET *ImgOut, *ImgY, *ImgCb, *ImgCr;

    lire_nb_lignes_colonnes_image_pgm(cNomImgY, &nH, &nW);
    nTaille = nH * nW;

    int nTaille3 = 3*nTaille;

    allocation_tableau(ImgOut, OCTET, nTaille3);
    allocation_tableau(ImgY, OCTET, nTaille);
    lire_image_pgm(cNomImgY, ImgY, nH * nW);
    allocation_tableau(ImgCb, OCTET, nTaille);
    lire_image_pgm(cNomImgCb, ImgCb, nH * nW);
    allocation_tableau(ImgCr, OCTET, nTaille);
    lire_image_pgm(cNomImgCr, ImgCr, nH * nW);


    for(int n=0; n<nTaille; n++){
      ImgOut[n*3    ] = (OCTET) MIN(255, MAX(0, (ImgY[n] + (1.402 * (ImgCr[n] - 128.)))));
      ImgOut[n*3 + 1] = (OCTET) MIN(255, 
        MAX(0, (ImgY[n] - (0.34414 * (ImgCb[n] - 128.)) - (0.714414*(ImgCr[n] - 128.))  )));
      ImgOut[n*3 + 2] = (OCTET) MIN(255, MAX(0, (ImgY[n] + (1.772 * (ImgCb[n] - 128.)))));
    }

    ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);

    free(ImgOut); free(ImgY); free(ImgCb); free(ImgCr);


	return 0;
}
