// YCbCrv2.cpp : image YCbCr to ppm

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgLue2[250], cNomImgLue3[250], cNomImgEcrite[250];
  int nH, nW, nTaille, Y, Cb, Cr;
  
  if (argc != 5) 
     {
       printf("Usage: Y.pgm Cb.pgm Cr.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgLue2) ;
   sscanf (argv[3],"%s",cNomImgLue3);
   sscanf (argv[4],"%s",cNomImgEcrite) ;

   OCTET *ImgIn,*ImgIn2, *ImgIn3, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);

   
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   
   allocation_tableau(ImgIn, OCTET, nTaille3);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   allocation_tableau(ImgIn3, OCTET, nTaille);
   
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
   lire_image_pgm(cNomImgLue3, ImgIn3, nH * nW);
   
   allocation_tableau(ImgOut, OCTET, nTaille3);
   
   for (int i=0; i < nTaille3; i+=3)
     {
       Y = ImgIn[i/3];
       Cb = ImgIn2[i/3];
       Cr = ImgIn3[i/3];
       /*
       ImgOut[i+2]= Y + 1.402*(Cr-128); //R
       ImgOut[i]=Y+1.772*(Cb-128); //B
       ImgOut[i+1]=Y-0.34414*(Cb-128)-0.714414*(Cr-128); //G
       */
       if(Y + 1.402*(Cr-128)<0) ImgOut[i]=0;
       else if(Y + 1.402*(Cr-128)>255) ImgOut[i]=255;
       else ImgOut[i]= Y + 1.402*(Cr-128); //R
       
       
       if(Y-0.34414*(Cb-128)-0.714414*(Cr-128)<0) ImgOut[i]=0;
       else if(Y-0.34414*(Cb-128)-0.714414*(Cr-128)>255) ImgOut[i]=255;
       else ImgOut[i+1]=Y-0.34414*(Cb-128)-0.714414*(Cr-128); //G
       
       
       if(Y+1.772*(Cb-128)<0) ImgOut[i]=0;
       else if(Y+1.772*(Cb-128)>255) ImgOut[i]=255;
       else ImgOut[i+2]=Y+1.772*(Cb-128); //B
       
     }

   
   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);free(ImgIn2);free(ImgIn3);
   return 1;
}
