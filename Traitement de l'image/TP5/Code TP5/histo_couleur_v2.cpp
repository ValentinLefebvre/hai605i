// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

/*
Lefebvre
Valentin
21604121
 */

#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  int histoR[256];
  int histoG[256];
  int histoB[256];
  
  if (argc != 2) 
     {
       printf("Usage: ImageIn.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;


   for(int i = 0; i < 256; i++){
    histoR[i] = 0;
    histoG[i] = 0;
    histoB[i] = 0;
    } 

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   
	
   for (int i=0; i < nTaille3; i+=3)
     {
       histoR[ImgIn[i]]++;
       histoG[ImgIn[i+1]]++;
       histoB[ImgIn[i+2]]++;
     }

   ofstream outfile ("histo_couleur2.dat");

  printf("indice | red | green | blue ");
  for(int i = 0; i < 256; i++){
    cout <<i <<"  | " << histoR[i]  << " | " << histoG[i] << " | " << histoB[i]<< endl;
    outfile << i << ' ' << histoR[i]  << ' ' << histoG[i] << ' ' << histoB[i]<< endl;
  }
  outfile.close();

   free(ImgIn);
   return 1;
}
