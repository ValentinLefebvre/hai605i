// RGBtoY.cpp : image ppm to pgm

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgLue2[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB, nH2, nW2;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.ppm ImageIn2.pgm ImageOut.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgLue2) ;
   sscanf (argv[3],"%s",cNomImgEcrite);

   OCTET *ImgIn,*ImgIn2, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue2, &nH2, &nW2);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH2 * nW2);
   allocation_tableau(ImgOut, OCTET, nTaille);
  
   for (int i=0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       ImgOut[i/3]=0.299*nR+0.587*nG+0.114*nB;
     }

   float err=0;
   for(int i=1;i<nH2*nW2;i++)
     err+=(pow(ImgIn2[i]-ImgOut[i],2));

   err=err/(nTaille3);
   printf("err = %f \n",err);

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;
}

