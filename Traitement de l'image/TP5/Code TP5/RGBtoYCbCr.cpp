// RGBtoYCbCr.cpp : image rgb to YCrCb

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250], cNomImgEcrite2[250], cNomImgEcrite3[250];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.ppm Y.pgm Cb.pgm Cr.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite) ;
   sscanf (argv[3],"%s",cNomImgEcrite2);
   sscanf (argv[4],"%s",cNomImgEcrite3) ;

   OCTET *ImgIn,*ImgOut, *ImgOut2, *ImgOut3;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   allocation_tableau(ImgOut, OCTET, nTaille);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut2, OCTET, nTaille);
   allocation_tableau(ImgOut3, OCTET, nTaille);
   
   for (int i=0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       ImgOut[i/3]=0.299*nR+0.587*nG+0.114*nB; //Y
       ImgOut2[i/3]=-0.1687*nR-0.3313*nG+0.5*nB + 128; //Cb
       ImgOut3[i/3]=0.5*nR-0.4187*nG-0.0813*nB + 128; //Cr
     }

   
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite2, ImgOut2,  nH, nW);
   ecrire_image_pgm(cNomImgEcrite3, ImgOut3,  nH, nW);
   free(ImgIn);
   return 1;
}
