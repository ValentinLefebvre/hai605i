// RGB2HSV.cpp : image ppm (RGB) to ppm (HSV)

#include <algorithm>
#include <stdio.h>
#include "image_ppm.h"

using namespace std;

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageInRGB.ppm V.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;

   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);

   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);

   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

   allocation_tableau(ImgOut, OCTET, nTaille);

   float nR, nG, nB;

    float h,s,v;
    float rgbMin, rgbMax;

    for (int i=0; i < nTaille3; i+=3)
     {
      nR = ImgIn[i]; //R
       nG = ImgIn[i+1]; //G
       nB = ImgIn[i+2]; //B

        rgbMin = nR < nG ? (nR < nB ? nR : nB) : (nG < nB ? nG : nB);
        rgbMax = nR > nG ? (nR > nB ? nR : nB) : (nG > nB ? nG : nB);

        v = rgbMax;
        if (v == 0)
        {
            h = 0;
            s = 0;
            //ImgOut[i]=h;
            //ImgOut[i+1]=s;
            ImgOut[(i+2)/3]=v;
        }

        s = 255 * long(rgbMax - rgbMin) / v;
        if (s == 0)
        {
            h = 0;
            //ImgOut[i]=h;
            //ImgOut[i+1]=s;
            ImgOut[(i+2)/3]=v;
        }

        if (rgbMax == nR)
            h = 0 + 43 * (nG - nB) / (rgbMax - rgbMin);
        else if (rgbMax == nG)
            h = 85 + 43 * (nB - nR) / (rgbMax - rgbMin);
        else
            h = 171 + 43 * (nR - nG) / (rgbMax - rgbMin);

        //ImgOut[i]=h;
        //ImgOut[i+1]=s;
        ImgOut[(i+2)/3]=v;
  }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;
}

