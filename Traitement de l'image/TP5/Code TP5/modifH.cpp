// modifH.cpp : image H.pgm to H modif.pgm

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, Y, Cb, Cr,k;
  
  if (argc != 4) 
     {
       printf("Usage: HSV.ppm int k HmodifSV.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2], "%d", &k);
   sscanf (argv[3],"%s",cNomImgEcrite) ;
   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);

   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);

   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

   allocation_tableau(ImgOut, OCTET, nTaille3);

   
   for (int i=0; i < nTaille3; i+=3)
     {
       ImgOut[i]=ImgIn[i];
       ImgOut[i+1]=ImgIn[i+1];
       ImgOut[i+2]=ImgIn[i+2];
     }
   int a = 0;
   for (int i=0; i < nTaille3; i+=3)
     {
       a=ImgIn[i]+k;
       a = (a+255) %255;
       ImgOut[i]= a;
       ImgOut[i+1]=ImgIn[i+1];
       ImgOut[i+2]=ImgIn[i+2];
      }

  

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;
}
