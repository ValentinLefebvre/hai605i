// mini.cpp : Réduire une image /4 

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
   int nMini = nTaille / 4 ;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nMini);
	
   printf("nTaille %d\n =",nTaille);
   printf("nTaille/4 = %d\n",nTaille/4);
   /*
   int k,m;
   k = 0;
   m = 0;
   while(k<nH/2){
    */
   for (int i=0; i < nH/2; i++)
   {
     for (int j=0; j < nW/2; j++)
       {
        //printf("test 1\n");
        ImgOut[i*nW+j]=ImgIn[i*nW+j];
        //m++;
        //printf("m = %d\n",m);
       }
    //k++;
    //printf("k = %d\n",k);
   }
 //}

   ecrire_image_pgm(cNomImgEcrite, ImgOut, nH/2, nW/2);
   free(ImgIn); free(ImgOut);

   return 1;
}
