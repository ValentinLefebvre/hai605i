// edv.cpp : utilisation de la carte des gradients et de la dilatation pour arriver à l'érosion

#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgLue2[250], cNomImgLue3[250], cNomImgEcrite[250];
  int nH, nW, nTaille, S;
  
  if (argc != 5) 
     {
       printf("Usage: Gradient_SB.pgm Seuil_SH.pgm Img_SH_dilatée.pgm ImgOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgLue2) ;
   sscanf (argv[3],"%s",cNomImgLue3) ;
   sscanf (argv[4],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgIn2, *ImgIn3, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   allocation_tableau(ImgIn3, OCTET, nTaille);

   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
   lire_image_pgm(cNomImgLue3, ImgIn3, nH * nW);

   allocation_tableau(ImgOut, OCTET, nTaille);


   for(int i=0;i<nH;i++)
     for(int j=0;j<nW;j++)
       ImgOut[i*nW+j]=ImgIn[i*nW+j];

   for(int i = 0 ; i < nTaille ; i ++)
   {
      if(ImgIn2[i]==255)
      {
       ImgOut[i] = 255;
     }
     else if(ImgIn[i]==255 && ImgIn3[i]==255)
     {
      ImgOut[i]=255;
     }
     else 
     {
      ImgOut[i] = 0;
     }
   }

   
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgIn2); free(ImgIn3); free(ImgOut);

   return 1;
}
