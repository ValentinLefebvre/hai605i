// Dithering : Algorithme de Floyd-Steinberg

#include <stdio.h>
#include <experimental/algorithm>
#include "image_ppm.h"
using namespace std;

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   for (int i=0; i < nH; i++)
    for (int j=0; j < nW; j++)
     {
      ImgOut[i*nW+j]=ImgIn[i*nW+j];
     }

     // cf https://martinsiesse.medium.com/impl%C3%A9mentation-dun-algorithme-simple-de-dithering-floyd-steinberg-dithering-42d765220036
     //int e,t,r;
     //e,t,r = 0;

     int S = 0;
     for(int i =0;i<nH;i++)
       for(int j=0;j<nW;j++)
         {
     S=S+ImgIn[i*nW+j];
         }

     S=S/(nH*nW);

    int e = 0;
    for (int i = 1; i < nH-1; i++) {
        for (int j = 1; j < nW-1; j++) {
          /*
          r = ImgIn[i*nW+j]/255;
            if(r<0.5)
              t = 0;
            else t = 1 ;

            e = r - t;

            e = e * 255;
*/
            if(ImgIn[i*nW+j]<S)
              e = ImgIn[i*nW+j];
            else e = ImgIn[i*nW+j] - S;
            /*
            if(e>255)
              e = 255;
            if(e<0)
              e = 0;
              */
            ImgOut[(i+1)*nW+j] = min(255,max(0,ImgOut[(i+1)*nW+j] + (e * 7/16)));
            ImgOut[(i+1)*nW+j+1] = min(255,max(0,ImgOut[(i+1)*nW+j+1] + (e * 3/16)));
            ImgOut[i*nW+j+1] = min(255,max(0,ImgOut[i*nW+j+1] + (e * 5/16)));
            ImgOut[(i-1)*nW+j+1] = min(255,max(0,ImgOut[(i-1)*nW+j+1] + (e * 1/16)));
      }
  }



/*
   for (int i=0; i < nH; i++)
    for (int j=0; j < nW; j++)
     {
      ImgOut[i*nW+j]=ImgIn[i*nW+j];
     }

    for (int i = 1; i < nH-1; i++) {
        for (int j = 1; j < nW-1; j++) {
                int P = trunc(ImgOut[i*nW+j]+0.5);
                double e = ImgOut[i*nW+j] - P;
                ImgOut[i*nW+j] = P;
                ImgOut[i*nW+j+1] += (e * 7/16);
                ImgOut[(i+1)*nW+j-1] += (e * 3/16);
                ImgOut[(i+1)*nW+j] += (e * 5/16);
                ImgOut[(i+1)*nW+j+1] += (e * 1/16);
      }
  }
*/

  /*
   int ap, np, err;

 
 for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
       int index = i + j * nH;
       int color = 0.299*nR+0.587*nG+0.114*nB;
     }
     
       ap = ImgIn[i]+ImgIn[i+1]+ImgIn[i+2];
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       np = 0.299*nR+0.587*nG+0.114*nB;
       ImgOut[i/3]=np;
       err = ap - np;


       if(i+nW<nTaille3) ImgOut[(i+nW)/3]+=(7/16)*err;
       if(i-nW+3>0) ImgOut[(i-nW+3)/3]+=(3/16)*err;
       if(i+3<nTaille3) ImgOut[(i/3)+1]+=(5/16)*err;
       if(i+nW+3<nTaille3) ImgOut[i+nW+3]+=(1/16)*err;
     }
*/
      

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;
}

