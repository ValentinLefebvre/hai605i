// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

/*
Lefebvre
Valentin
21604121
 */

bool voisinsmoinsun(OCTET* img,int i,int j, int nH, int nW) {
  if (i == 0 || i == nH-1 || j == 0 || j == nW-1) {return false;}
  if (img[(i-1)*nW+(j-1)] ==-1) return true;
  if (img[(i)*nW+(j-1)] == -1) return true;
  if (img[(i+1)*nW+(j-1)] == -1) return true;
  if (img[(i-1)*nW+(j)] == -1) return true;
  if (img[(i)*nW+(j)] == -1) return true;
  if (img[(i+1)*nW+(j)] == -1) return true;
  if (img[(i-1)*nW+(j+1)] == -1) return true;
  if (img[(i)*nW+(j+1)] == -1) return true;
  if (img[(i+1)*nW+(j+1)] == -1) return true;
  return false;
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: Gradient.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   int label[nTaille];
   for(int i = 0; i < nTaille ; i++)
    label[i]=-1;

  int label_equivalents[nTaille];

  int label_courant = 0;

  //premiere passe
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
      	 if(ImgIn[i*nW+j]==0)
         {
          int a,b;
          a,b=0;
          if(!voisinsmoinsun(ImgIn,i,j,nH,nW))
          {
              if (img[(i-1)*nW+(j-1)] ==-1) {a=i;b=j;}
              if (img[(i)*nW+(j-1)] == -1) {a=i;b=j;}
              if (img[(i+1)*nW+(j-1)] == -1) {a=i;b=j;}
              if (img[(i-1)*nW+(j)] == -1) {a=i;b=j;}
              if (img[(i)*nW+(j)] == -1) {a=i;b=j;}
              if (img[(i+1)*nW+(j)] == -1) {a=i;b=j;}
              if (img[(i-1)*nW+(j+1)] == -1) {a=i;b=j;}
              if (img[(i)*nW+(j+1)] == -1) {a=i;b=j;}
              if (img[(i+1)*nW+(j+1)] == -1) {a=i;b=j;}
              label[i*nW+j]=label[a*nW+j];
              label_equivalents[label[]]

        }
          
          /*
                 Si plusieurs voisins ont un label different :
                     Labels_equivalents[label le plus petit des voisins] = union de tous les labels des voisins
         Sinon :
             label_courant = label_courant + 1;
             Labels[i, j] = label_courant;
            Fin POUR
          Fin POUR
          */
         }

	     }
		    
   //deuxième passe
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
		    
       }

   
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
