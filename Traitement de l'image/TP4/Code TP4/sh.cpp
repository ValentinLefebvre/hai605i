// seuillage_hysteresis.cpp : Seuille une image en niveau de gris (Seuil bas Seuil Haut)

#include <stdio.h>
#include "image_ppm.h"
/*
Lefebvre
Valentin
21604121
 */


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, SB, SH;
  
  if (argc != 5) 
     {
       printf("Usage: Gradient.pgm Sh.pgm SeuilBas SeuilHaut \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&SB);
   sscanf (argv[4],"%d",&SH);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       ImgOut[i*nW+j]=ImgIn[i*nW+j];
 /*
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
      	 if ( ImgIn[i*nW+j] <= SB) 
          ImgOut[i*nW+j]=0;
      	 else if ( ImgIn[i*nW+j] >=SH) 
          ImgOut[i*nW+j]=255;
       }
       for (int i=1; i < nH-1; i++)
     for (int j=1; j < nW-1; j++)
      {
         if((ImgIn[i*nW+j]>SB) && ImgIn[i*nW+j]<SH)
         {
           if(ImgIn[i*nW+j-nW-1]==255 || 
            ImgIn[i*nW+j-nW+1]==255 ||
             ImgIn[i*nW+j-nW]==255 ||
              ImgIn[i*nW+j+nW]==255 ||
               ImgIn[i*nW+j-1]==255 || 
               ImgIn[i*nW+j+1]==255 || 
               ImgIn[i*nW+nW+j-1]==255 || 
               ImgIn[i*nW+j+nW+1]==255)
           {
             ImgOut[i*nW+j]=255;
           }
       else ImgOut[i*nW+j]=0;
         }
       }
*/

int res[nTaille];
for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
        res[i*nW+j]=ImgIn[i*nW+j];
      }

for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
         if ( ImgIn[i*nW+j] <= SB) 
          res[i*nW+j]=0;
         else if ( ImgIn[i*nW+j] >=SH) 
          res[i*nW+j]=255;
       }

   for (int i=1; i < nH-1; i++)
     for (int j=1; j < nW-1; j++)
      {
    	   if((res[i*nW+j]>SB) && res[i*nW+j]<SH)
         {
    	     if((res[(i-1)*nW+j-1]==255) || 
            (res[(i-1)*nW+j+1]==255) ||
             (res[(i-1)*nW+j]==255) ||
               (res[i*nW+j-1]==255) || 
               (res[i*nW+j+1]==255) || 
               (res[(i+1)*nW+j]==255) ||
               (res[(i+1)*nW+j-1]==255) || 
               (res[(i+1)*nW+j+1]==255))
           {
    	       ImgOut[i*nW+j]=255;
	         }
	     else ImgOut[i*nW+j]=0;
	       }
       }
 
    /*
  for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
         if ( ImgIn[i*nW+j] <= SB) 
          ImgIn[i*nW+j]=0;
         else if ( ImgIn[i*nW+j] >=SH) 
          ImgIn[i*nW+j]=255;
       }

  for (int i=1; i < nH-1; i++)
     for (int j=1; j < nW-1; j++)
      {
         if((ImgIn[i*nW+j]>SB) && ImgIn[i*nW+j]<SH)
         {
           if((ImgIn[(i-1)*nW+j-1]==255) || 
            (ImgIn[(i-1)*nW+j+1]==255) ||
             (ImgIn[(i-1)*nW+j]==255) ||
               (ImgIn[i*nW+j-1]==255) || 
               (ImgIn[i*nW+j+1]==255) || 
               (ImgIn[(i+1)*nW+j]==255) ||
               (ImgIn[(i+1)*nW+j-1]==255) || 
               (ImgIn[(i+1)*nW+j+1]==255))
           {
             ImgOut[i*nW+j]=255;
           }
       else ImgOut[i*nW+j]=0;
         }
       }

       */     
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
