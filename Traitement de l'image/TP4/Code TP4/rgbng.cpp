// RGBtoY.cpp : image ppm to pgm

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250],cNomImgLue2[250],cNomImgLue3[250],cNomImgLue4[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB;
  
  if (argc != 6) 
     {
       printf("Usage: ImageIn.ppm iX.pgm iY.pgm Gradient.pgm ImageOut.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgLue2) ;
   sscanf (argv[3],"%s",cNomImgLue3) ;
   sscanf (argv[4],"%s",cNomImgLue4) ;
   sscanf (argv[5],"%s",cNomImgEcrite);

   OCTET *ImgIn,*ImgIn2,*ImgIn3,*ImgIn4, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);

   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;

   allocation_tableau(ImgIn, OCTET, nTaille3);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   allocation_tableau(ImgIn3, OCTET, nTaille);
   allocation_tableau(ImgIn4, OCTET, nTaille);

   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
   lire_image_pgm(cNomImgLue3, ImgIn3, nH * nW);
   lire_image_pgm(cNomImgLue4, ImgIn4, nH * nW);

   allocation_tableau(ImgOut, OCTET, nTaille3);


   for (int i=0; i < nTaille3; i+=3)
     {
      ImgOut[i]=ImgIn2[i/3];
      ImgOut[i+1]=ImgIn3[i/3];
      ImgOut[i+2]=ImgIn4[i/3]; //255; //ImgIn4[i/3];
     }
/*
  
   for (int i=0; i < nTaille3; i+=3)
     {

       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       ic=0.299*nR+0.587*nG+0.114*nB;


      iX,iY=0;
      if(i+nW<nTaille3) 
        {
          is = 0;
          nR = ImgIn[i+nW];
          nG = ImgIn[i+nW+1];
          nB = ImgIn[i+nW+2];
          is=0.299*nR+0.587*nG+0.114*nB;
          iX = is - ic;
        }


      if(i+3<nTaille3) 
      {
         is = 0;
         nR = ImgIn[i+3];
         nG = ImgIn[i+4];
         nB = ImgIn[i+5];
         is=0.299*nR+0.587*nG+0.114*nB;
         iY = is - ic;
      }

       ImgOut[i]=iX;
       ImgOut[i+1]=iY;
       ImgOut[i+2]=255;

     }
*/
   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgIn2); free(ImgIn3); free(ImgIn4); free(ImgOut);
   return 1;
}

