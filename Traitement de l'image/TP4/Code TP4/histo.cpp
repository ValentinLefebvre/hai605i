#include <stdio.h>
#include <iostream>
#include "image_ppm.h"
#include <iostream>
#include <fstream>  

/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille;
  std::cout << "You have entered " << argc
         << " arguments:" << "\n";
  
  if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

   
   int histo[256];
   for(int i=0;i<256;i++)
     histo[i]=0;
   
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       histo[ImgIn[i*nW+j]]++;

   std::cout<<"indice | occurence";
   for(int i=0;i<256;i++)
     {
       std::cout<<i<<" "<<histo[i];
       std::cout<<std::endl;
     }
   int cpt=0;
   for(int i=0;i<256;i++)
     cpt=cpt+histo[i];
   
   std::cout<<"Test"<<std::endl;
   std::cout<<cpt<<std::endl;
   std::cout<<nH*nW<<std::endl;

   std::ofstream outfile ("histo.dat");

   for(int i=0;i<255;i++)
     outfile<<i<<" "<<histo[i]<<std::endl;

   outfile.close();
   
   return 1;
}
