// sh2.cpp : Seuille une image en niveau de gris (Seuil bas Seuil Haut), hystérésis complète

#include <stdio.h>
#include "image_ppm.h"
/*
Lefebvre
Valentin
21604121
 */

bool voisins(OCTET* img,int i,int j, int nH, int nW) {
  if (i == 0 || i == nH-1 || j == 0 || j == nW-1) {return false;}
  if (img[(i-1)*nW+(j-1)] == 255) return true;
  if (img[(i)*nW+(j-1)] == 255) return true;
  if (img[(i+1)*nW+(j-1)] == 255) return true;
  if (img[(i-1)*nW+(j)] == 255) return true;
  if (img[(i)*nW+(j)] == 255) return true;
  if (img[(i+1)*nW+(j)] == 255) return true;
  if (img[(i-1)*nW+(j+1)] == 255) return true;
  if (img[(i)*nW+(j+1)] == 255) return true;
  if (img[(i+1)*nW+(j+1)] == 255) return true;
  return false;
}

bool voisinssup(OCTET* img,int i,int j, int nH, int nW, int s) {
  if (i == 0 || i == nH-1 || j == 0 || j == nW-1) {return false;}
  if (img[(i-1)*nW+(j-1)] >= s) return true;
  if (img[(i)*nW+(j-1)] >= s) return true;
  if (img[(i+1)*nW+(j-1)] >= s) return true;
  if (img[(i-1)*nW+(j)] >= s) return true;
  if (img[(i)*nW+(j)] >= s) return true;
  if (img[(i+1)*nW+(j)] >= s) return true;
  if (img[(i-1)*nW+(j+1)] >= s) return true;
  if (img[(i)*nW+(j+1)] >= s) return true;
  if (img[(i+1)*nW+(j+1)] >= s) return true;
  return false;
}

bool voisinszero(OCTET* img,int i,int j, int nH, int nW) {
  if (i == 0 || i == nH-1 || j == 0 || j == nW-1) return false;
  return (img[(i-1)*nW+(j-1)] ==0) && (img[(i)*nW+(j-1)] ==0) &&  (img[(i+1)*nW+(j-1)]==0) && 
  (img[(i-1)*nW+(j)] ==0) && (img[(i)*nW+(j)] == 0) && (img[(i+1)*nW+(j)] ==0) && 
  (img[(i-1)*nW+(j+1)] == 0) && (img[(i)*nW+(j+1)] ==0) && (img[(i+1)*nW+(j+1)] ==0);
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, SB, SH;
  
  if (argc != 5) 
     {
       printf("Usage: Gradient.pgm SH.pgm SeuilBas SeuilHaut \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&SB);
   sscanf (argv[4],"%d",&SH);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   
 
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
      	 if (ImgIn[i*nW+j] < SH) 
          {
            ImgOut[i*nW+j]=0;
          } 
        else 
        {
          ImgOut[i*nW+j]=255;
        }
       }

  bool res = false;
  while(!res)
  {
    /*
   for (int i=1; i < nH-1; i++)
     for (int j=1; j < nW-1; j++)
       {
        res = true;
      	if((ImgOut[i*nW+j]>=SB) && ImgOut[i*nW+j]<=SH)
           {
            if(ImgOut[i*nW+j-nW-1]>=SB || 
              ImgOut[i*nW+j-nW+1]>=SB ||
               ImgOut[i*nW+j-nW]>=SB ||
                ImgOut[i*nW+j+nW]>=SB ||
                 ImgOut[i*nW+j-1]>=SB || 
                 ImgOut[i*nW+j+1]>=SB || 
                 ImgOut[i*nW+nW+j-1]>=SB || 
                 ImgOut[i*nW+j+nW+1]>=SB)
             {
              res = false;
            }
      	  if(voisins(ImgOut,i,j,nH,nW)) 
             {
      	       ImgOut[i*nW+j]=255;
	           }
             if(ImgOut[i*nW+j-nW-1]==0 && 
              ImgOut[i*nW+j-nW+1]==0 &&
               ImgOut[i*nW+j-nW]==0 &&
                ImgOut[i*nW+j+nW]==0 &&
                 ImgOut[i*nW+j-1]==0 && 
                 ImgOut[i*nW+j+1]==0 && 
                 ImgOut[i*nW+nW+j-1]==0 && 
                 ImgOut[i*nW+j+nW+1]==0)
             {
               ImgOut[i*nW+j]=0;
             }
      	   }
      	 }
       }
       */
    res = true;
    for (int i=0; i < nH; i++) 
     for (int j=0; j < nW; j++) 
    {
        if((ImgOut[i*nW+j]>=SB) && ImgOut[i*nW+j]<=SH && voisinssup(ImgOut,i,j,nH,nW,SB))
        {
          res = false;
        }
        if((ImgOut[i*nW+j]>=SB) && ImgOut[i*nW+j]<=SH && voisins(ImgOut,i,j,nH,nW))
          {
            ImgOut[i*nW+j]=255;
          }
        if((ImgOut[i*nW+j]>=SB) && ImgOut[i*nW+j]<=SH && voisinszero(ImgOut,i,j,nH,nW))
        {
          ImgOut[i*nW+j]=0;
        }
    }
  }
       
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
