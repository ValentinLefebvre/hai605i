#include <stdio.h>
#include <iostream>
#include "image_ppm.h"
#include <iostream>
#include <fstream>  

/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille, cl, nl;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ligne=0/colonne=1 numligne/colonne \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%d",&cl); //ligne ou colonne
   sscanf (argv[3],"%d",&nl); //numéro ligne ou colonne

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

   
   int profil[256];
   for(int i=0;i<256;i++)
     profil[i]=0;
   if(cl==0)
     {
       for(int j=0;j<nW;j++)
	 profil[j]=ImgIn[nl*nW+j];
     }
   else
     {
       for(int i=0;i<nH;i++)
	 profil[i]=ImgIn[i*nW+nl];
     }
		
   std::ofstream outfile ("profil.dat");

   for(int i=0;i<255;i++)
     outfile<<i<<" "<<profil[i]<<std::endl;

   outfile.close();
   
   return 1;
}
