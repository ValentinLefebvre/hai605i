// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgLue2[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 4) 
     {
       printf("Usage: Lena.pgm Lena_binary.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgLue2) ;
   sscanf (argv[3],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut, *ImgIn2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue2, &nH, &nW);
   
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
   
   allocation_tableau(ImgOut, OCTET, nTaille);

 for (int i=1; i < nH-1; i++)
   for (int j=1; j < nW-1; j++)
     {
      ImgOut[i*nW+j]=ImgIn[i*nW+j];
    }
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
      	   ImgOut[i*nW+j]=
             (ImgIn[i*nW+j]+
              ImgIn[(i-1)*nW+j]+
              ImgIn[(i+1)*nW+j]+
              ImgIn[i*nW+j-1]+
              ImgIn[i*nW+j+1]+
              ImgIn2[i*nW+j]+
              ImgIn2[(i-1)*nW+j]+
              ImgIn2[(i+1)*nW+j]+
              ImgIn2[i*nW+j-1]+
              ImgIn2[i*nW+j+1])
             /10;   		    
       }
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgIn2); free(ImgOut);

   return 1;
}
