// blur.cpp : Floute une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

/*
Lefebvre
Valentin
21604121
*/
int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, R;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm int R(coeff distance) \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&R);

   printf("R = %d\n",R);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for (int i=1; i < nH-1; i++)
    for (int j=1; j < nW-1; j++)
     {
    ImgOut[i*nW+j]= ImgIn[i*nW+j];
    }
    /*
    int x_centre = R;
    int y_centre = R;
    int n;
    int distance;
    */
    int res;
    int n;
     for (int i=1; i < nH-1; i++)
       for (int j=1; j < nW-1; j++)
         {
          res = 0;
          n = 0;
          //ImgOut[i*nW+j]=0;
           //x_centre = i;
          // y_centre = j;
           for(int k = - R; k < R + 1; k ++)
            for(int l = - R; l < R + 1; l ++)
              {
                if(i+k>0 && j+l >0 && i+k<nH-1 && j+l<nW-1)
                {
                  res=res+ImgIn[(i+k)*nW+j+l];
                  n++;
                }
              }
          ImgOut[i*nW+j]=res/n;
         }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);free(ImgOut);
   return 1;
}
