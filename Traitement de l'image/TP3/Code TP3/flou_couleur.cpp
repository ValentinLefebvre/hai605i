// blur.cpp : Floute une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

/*
Lefebvre
Valentin
21604121
*/
int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  int som;
  if (argc != 3) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);

    for (int i=0; i < nTaille3; i++)
      ImgOut[i]= ImgIn[i];
    
   for (int i=nW; i < nTaille3; i+=3)
     {
       ImgOut[i]=(ImgIn[i]+ImgIn[i+nW*3]+ImgIn[i-nW*3]+ImgIn[i-3]+ImgIn[i+3])/5;
       ImgOut[i+1]=(ImgIn[i+1]+ImgIn[i+nW*3+1]+ImgIn[i-nW*3+1]+ImgIn[i-3+1]+ImgIn[i+3+1])/5;
       ImgOut[i+2]=(ImgIn[i+2]+ImgIn[i+nW*3+2]+ImgIn[i-nW*3+2]+ImgIn[i-3+2]+ImgIn[i+3+2])/5;
       

     }

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;

}
