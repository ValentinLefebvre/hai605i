#include <stdio.h>
#include <iostream>
#include "image_ppm.h"
#include <iostream>
#include <fstream>  

/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille;
  
  if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   
   
   int histo_r[256];
   int histo_g[256];
   int histo_b[256];
   for(int i=0;i<256;i++)
     {
       histo_r[i]=0;
       histo_g[i]=0;
       histo_b[i]=0;
     }
	 
       
   
   for (int i=0; i < nTaille3; i+=3)
       {
	 histo_r[ImgIn[i]]++;
	 histo_g[ImgIn[i+1]]++;
	 histo_b[ImgIn[i+2]]++;
       }

   std::ofstream outfile ("histo_couleur.dat");

   for(int i=0;i<256;i++)
     {
       //std::cout<<i<<" "<<histo_r[i]<<" "<<histo_c[i+1]<<" "<<histo_c[i+2]<<std::endl;  
     outfile<<i<<" "<<histo_r[i]<<" "<<histo_g[i]<<" "<<histo_b[i]<<std::endl;
     }

   outfile.close();
 
   
   return 1;
}
