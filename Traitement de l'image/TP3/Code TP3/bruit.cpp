// blur.cpp : Floute une image en niveau de gris

#include <stdio.h>
#include <cstdlib>
#include <time.h>
#include "image_ppm.h"

/*
Lefebvre
Valentin
21604121
*/
int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  srand(time(NULL));
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   //int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i=0; i < nTaille; i++)
      ImgOut[i]= ImgIn[i];

    int res,chance;

    for (int i=0; i < nH; i++)
      for (int j=0;j < nW; j++)
     {
       chance = rand() % 50 + 1;
       if(chance==50){
	 res = rand() % 255 + 1;
	 ImgOut[i*nW+j]=(ImgIn[i*nW+j]+res)%255;
	 }
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;

}
