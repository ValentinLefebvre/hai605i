// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"
/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int Seuils[256];
  int nH, nW, nTaille, nbseuils;

  for(int i=0;i<256;i++)
    Seuils[i]=255;
  
  if (argc < 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm nbseuils Seuil1 Seuil2 Seuil3 ... Seuiln\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&nbseuils);

   for(int i=4;i<nbseuils+4;i++)
    sscanf (argv[i],"%d",&Seuils[i-4]);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	


   for (int i=0; i < nH; i++)
    for (int j=0; j < nW; j++)
      {
        ImgOut[i*nW+j]=ImgIn[i*nW+j];
      }

   int k = 0;
 for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
      k=0;
      while(ImgIn[i*nW+j]>=Seuils[k])
        {k++;}
      
      if(k==0) ImgOut[i*nW+j]=0;
      else {
        if(ImgIn[i*nW+j]>Seuils[nbseuils-1]) ImgOut[i*nW+j]=255;
          else ImgOut[i*nW+j]=((Seuils[k-1]+Seuils[k])/2);
     }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
