// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <algorithm>
#include "image_ppm.h"

/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

    for(int i =0;i<nH;i++)
     for(int j=0;j<nW;j++)
       {
	 ImgOut[i*nW+j]=ImgIn[i*nW+j];
       }
    
   int med[256];
   int S = 0;
   for(int i =0;i<nH;i++)
     for(int j=0;j<nW;j++)
       {
	 med[ImgIn[i*nW+j]]++;
       }
   std::sort(med,med+256);
   //for(int i = 0 ; i<256; i++)
   //  med[i]=med[i]/nTaille;
   // std::cout<<med[256/2]<<std::endl;
   std::cout<<(med[256/2]+med[(256/2)-1])/2<<std::endl;
   S=(med[256/2]+med[(256/2)-1])/2;
   S = ImgIn[S];
   std::cout<<S<<std::endl;
   
   
 for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
       if ( ImgIn[i*nW+j] < S) ImgOut[i*nW+j]=0; else ImgOut[i*nW+j]=255;
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
