// erosion.cpp : supprimer les points objets isolés

#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   //std::cout<<"nW = "<<nW<<" et nH = "<<nH<<std::endl;
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }

   for(int i=0;i<nH;i++)
     for(int j=0;j<nW;j++)
       ImgOut[i*nW+j]=ImgIn[i*nW+j];

   // std::cout<<"test"<<std::endl;
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
	 if(ImgIn[i*nW+j]==0)
	   {
	     
	     if(j!=0) ImgOut[(i*nW+j) -1]=0;
	     if(j!=nH-1) ImgOut[(i*nW+j) +1]=0;
	     if(i!=nW-1) ImgOut[(i*nW+j) + nW]=0;
	     if(i!=0) ImgOut[(i*nW+j) - nW]=0;
	   }
		    
       }

   //std::cout<<"test v3"<<std::endl;
   
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
