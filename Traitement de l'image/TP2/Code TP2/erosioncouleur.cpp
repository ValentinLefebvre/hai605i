// erosion.cpp : supprimer les points objets isolés

#include <stdio.h>
#include <iostream>
#include "image_ppm.h"

/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);

   

   for(int i=0;i<nH;i++)
     for(int j=0;j<nW;j++)
       {
        ImgOut[i*nW+j]=ImgIn[i*nW+j];
      }

    int nR, nG, nB, SR, SG, SB;
    SR,SG,SB = 0;

   for (int i=0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];
       SR = SR + nR;
       SG = SG + nG;
       SB = SB + nB;
     }
     /*
   printf("SR =%d\n",SR);
   printf("SG =%d\n",SG);
   printf("SB =%d\n",SB);
*/
   SR=SR/(nTaille);
   SG=SG/(nTaille);
   SB=SB/(nTaille);

   printf("SR =%d\n",SR);
   printf("SG =%d\n",SG);
   printf("SB =%d\n",SB);
   
   if(SR<0) SR=0;
   if(SG<0) SG=0;
   if(SB<0) SB=0;

   if(SR>255) SR=255;
   if(SG>255) SG=255;
   if(SG>255) SG=255;
/*
   printf("SR =%d\n",SR);
   printf("SG =%d\n",SG);
   printf("SB =%d\n",SB);
*/
   for (int i=0; i < nTaille3; i+=3)
     {
       nR = ImgIn[i];
       nG = ImgIn[i+1];
       nB = ImgIn[i+2];

        if(nR<SR)
         {
           if(i!=0) ImgOut[i-3]=ImgIn[i];
           if(i!=(nTaille3-1)) ImgOut[i+3]=ImgIn[i];
           if((i+nW)<nTaille3) ImgOut[i+nW]=ImgIn[i];
           if((i-nW)>0) ImgOut[i - nW]=ImgIn[i];
           ImgOut[i]=ImgIn[i];
         }
         else 
         {
          ImgOut[i]=ImgIn[i];
         }


         if(nG<SG)
           {
             if(i!=0) ImgOut[i-2]=ImgIn[i+1];
             if(i!=(nTaille3-1)) ImgOut[i+4]=ImgIn[i+1];
             if(i+nW<nTaille3) ImgOut[i+1+nW]=ImgIn[i+1];
             if(i-nW>0) ImgOut[i + 1 - nW]=ImgIn[i+1];
             ImgOut[i+1]=ImgIn[i+1];
           }
          else 
           {
            ImgOut[i+1]=ImgIn[i+1];
           }


         if(nB<SB)
           {
             if(i!=0) ImgOut[i-1]=ImgIn[i+2];
             if(i!=(nTaille3-1)) ImgOut[i+5]=ImgIn[i+2];
             if(i+nW<nTaille3) ImgOut[i+2+nW]=ImgIn[i+2];
             if(i-nW>0) ImgOut[i + 2 - nW]=ImgIn[i+2];
             ImgOut[i+2]=ImgIn[i+2];
           }
         else 
         {
          ImgOut[i+2]=ImgIn[i+2];
         }
     }


   
   
   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
