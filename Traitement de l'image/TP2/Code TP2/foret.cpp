#include <stdio.h>
#include <iostream>
#include "image_ppm.h"
#include <iostream>
#include <fstream>  

/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille;
  /*std::cout << "You have entered " << argc
         << " arguments:" << "\n";
  */
  if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

   
   int nB = 0;
   int nN = 0 ;
   
   printf("nTaille = %d\n",nTaille);
   
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
        if(ImgIn[i*nW+j]==255)
        {
        nB++;
        }
        else
        {
         nN++;
       }
    }

   printf("Test : %d\n",nN+nB);
   printf("Pixels blanc = %d\n",nB);
   printf("Pixels noirs = %d\n",nN);

   return 1;
}
