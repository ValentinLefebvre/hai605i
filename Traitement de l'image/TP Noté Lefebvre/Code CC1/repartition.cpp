#include <stdio.h>
#include <iostream>
#include "image_ppm.h"
#include <iostream>
#include <fstream>  
//repartition.cpp
using namespace std;
/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille;
  /*
  std::cout << "You have entered " << argc
         << " arguments:" << "\n";
  */
  if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

   
   float histo[256];
   for(int i=0;i<256;i++)
     histo[i]=0;
   
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       histo[ImgIn[i*nW+j]]++;
  
   float ddp[256];
    for (int i=0; i < 256; i++)
       ddp[i]=((float)histo[i]/nTaille);

    float repart[256];
    repart[0]=ddp[0];
    for(int i = 1 ; i<256;i++)
      repart[i]=repart[i-1]+ddp[i];
    
   ofstream outfile ("repart.dat");

   for(int i=0;i<255;i++)
     outfile<<i<<' '<<repart[i]<<endl;

   outfile.close();
   free(ImgIn);
   
   return 1;
}
