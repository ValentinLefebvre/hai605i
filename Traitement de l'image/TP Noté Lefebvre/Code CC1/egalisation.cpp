#include <stdio.h>
#include <iostream>
#include "image_ppm.h"
#include <iostream>
#include <fstream>
#include <math.h>
//egalisation.cpp
using namespace std;
/*
Lefebvre
Valentin
21604121
 */

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  /*
  std::cout << "You have entered " << argc
         << " arguments:" << "\n";
  */
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite) ;

   OCTET *ImgIn,*ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgOut, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

   
   float histo[256];
   for(int i=0;i<256;i++)
     histo[i]=0;
   
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       histo[ImgIn[i*nW+j]]++;
  
   float ddp[256];
    for (int i=0; i < 256; i++)
       ddp[i]=((float)histo[i]/nTaille);

    float repart[256];
    repart[0]=ddp[0];
    for(int i = 1 ; i<256;i++)
      repart[i]=repart[i-1]+ddp[i];

    for (int i=0; i < nH; i++)
      for (int j=0; j < nW; j++)
	 ImgOut[i*nW+j]=ImgIn[i*nW+j];

    //cout<<"Test"<<floor(3.2)<<endl;
    
    for (int i=0; i < nH; i++)
      for (int j=0; j < nW; j++)
	ImgOut[i*nW+j]=floor(repart[ImgIn[i*nW+j]]*255);
    
   ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
   free(ImgIn);free(ImgOut);
   
   return 1;
}
