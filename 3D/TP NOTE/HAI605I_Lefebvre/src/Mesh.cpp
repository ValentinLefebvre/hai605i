/*
Lefebvre Valentin
21604121
*/

#include "Mesh.h"
#include <algorithm>

using namespace std;

void Mesh::clear () {
    clearTopology ();
    clearGeometry ();
}

void Mesh::clearGeometry () {
    vertices.clear ();
}

void Mesh::clearTopology () {
    triangles.clear ();
}

void Mesh::computeTriangleNormals (vector<Vec3> & triangleNormals) {
    for (vector<Triangle>::const_iterator it = triangles.begin ();
         it != triangles.end ();
         it++) {
        Vec3 e01 (vertices[it->getVertex (1)].getPosition ()
                - vertices[it->getVertex (0)].getPosition ());
        Vec3 e02 (vertices[it->getVertex (2)].getPosition ()
                - vertices[it->getVertex (0)].getPosition ());
        Vec3 n (Vec3::crossProduct (e01, e02));
        n.normalize ();
        triangleNormals.push_back (n);
    }
}

void Mesh::recomputeSmoothVertexNormals (unsigned int normWeight) {
    vector<Vec3> triangleNormals;
    computeTriangleNormals (triangleNormals);
    for (vector<Vertex>::iterator it = vertices.begin ();
         it != vertices.end ();
         it++)
        it->setNormal (Vec3 (0.0, 0.0, 0.0));
    vector<Vec3>::iterator itNormal = triangleNormals.begin ();
    vector<Triangle>::iterator it = triangles.begin ();
    for ( ; it != triangles.end (); it++, itNormal++)
        for (unsigned int  j = 0; j < 3; j++) {
            Vertex & vj = vertices[it->getVertex (j)];
            float w = 1.0; // uniform weights
            Vec3 e0 = vertices[it->getVertex ((j+1)%3)].getPosition ()
                    - vj.getPosition ();
            Vec3 e1 = vertices[it->getVertex ((j+2)%3)].getPosition ()
                    - vj.getPosition ();
            if (normWeight == 1) { // area weight
                w = Vec3::crossProduct (e0, e1).getLength () / 2.0;
            } else if (normWeight == 2) { // angle weight
                e0.normalize ();
                e1.normalize ();
                w = (2.0 - (Vec3::dotProduct (e0, e1) + 1.0)) / 2.0;
            }
            if (w <= 0.0)
                continue;
            vj.setNormal (vj.getNormal () + (*itNormal) * w);
        }
    Vertex::normalizeNormals (vertices);
}

//one-ring of each vertex, i.e. a list of vertices with which it shares an edge
void Mesh::collectOneRing (vector<vector<unsigned int> > & oneRing) const {
    //Initialisation le vecteur de o_one_ring de la taille du vecteur vertices
    oneRing.resize (vertices.size ());
    //Parcours les triangles et ajoute les voisins dans le 1-voisinage
    //Tous les points opposés dans le triangle sont reliés
    for (unsigned int i = 0; i < triangles.size (); i++) {
        const Triangle & ti = triangles[i];
        for (unsigned int j = 0; j < 3; j++) {
            unsigned int vj = ti.getVertex (j);
            for (unsigned int k = 1; k < 3; k++) {
                unsigned int vk = ti.getVertex ((j+k)%3);
                if (find (oneRing[vj].begin (), oneRing[vj].end (), vk) == oneRing[vj].end ())
                    oneRing[vj].push_back (vk);
            }
        }
    }
}

void Mesh::addNoise(){
    for( unsigned int i = 0 ; i < vertices.size() ; i ++ )
    {
        float factor = 0.03;
        const Vec3 & p = vertices[i].position;
        const Vec3 & n = vertices[i].normal;
        vertices[i].position = Vec3( p[0] + factor*((double)(rand()) / (double)(RAND_MAX))*n[0],
                p[1] + factor*((double)(rand()) / (double)(RAND_MAX))*n[1],
                p[2] + factor*((double)(rand()) / (double)(RAND_MAX))*n[2]);

    }

}

//Fonction à completer Exercice 1
void Mesh::smooth( float lambda ){
    //Utiliser la fonction collectOneRing pour récuperer le 1-voisinage

    //(1) (a) Initialisation d'un vecteur pour les positions moyennes avec des coordonnées nulles


    //Parcourir les sommets


    //Parcourir le vecteur des positions resultante et diviser par le nombre de voisins (taille du vecteur de OneRing du sommet courant)


    // Parcourir les sommets et mettre à jour les positions en faisant un déplacement vers le barycentre du 1-voisinage

        // (1) (b) Calcul du vecteur de déplacement (position du sommet courant  vers la position moyenne)

        // (2) Déplacer le sommet de factor dans la direction calculée (nouvelle position = position courante + factor * vecteur)

//Question 1 a)
    //float delta_L[vertices.size()];
    //vector<unsigned int> delta_L;
    vector<vector<unsigned int>> oneRing ;

    collectOneRing(oneRing);

    vector<Vec3> voisins ;
    Vec3 somme = Vec3(0,0,0);
    Vec3 mi = Vec3(0,0,0);
    unsigned int nb; // où nb sommets voisins
    
    for(int i= 0 ; i < getVertices().size() ; ++i ) {
        //voisins = oneRing[i];
        Vec3 A = Vec3(0,0,0);
        for(int j = 0 ; j < oneRing[i].size() ; ++j) {
            //res += voisins[j].getPosition() ;
            A += getVertices()[oneRing[i][j]].getPosition(); //où A barycentre
            nb = oneRing[i].size();
        }
        mi = (1./(float)nb) * A;
        voisins.push_back(mi);
    }

    Vec3 Li = Vec3(0,0,0);
    vector<Vec3> T; // où T delta Li
    
    for(int i = 0 ; i < getVertices().size() ; ++i) {
       Li = voisins[i] - getVertices()[i].getPosition();
       T.push_back(Li);
    }

    for(int i = 0 ; i < getVertices().size() ; ++i){
        getVertices()[i].setPosition(getVertices()[i].getPosition() + lambda*T[i]);
    }  
    
    ////////////////////
    
    

    
}
