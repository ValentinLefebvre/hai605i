// --------------------------------------------------------------------------
// gMini,
// a minimal Glut/OpenGL app to extend                              
//
// Copyright(C) 2007-2009                
// Tamy Boubekeur
//                                                                            
// All rights reserved.                                                       
//                                                                            
// This program is free software; you can redistribute it and/or modify       
// it under the terms of the GNU General Public License as published by       
// the Free Software Foundation; either version 2 of the License, or          
// (at your option) any later version.                                        
//                                                                            
// This program is distributed in the hope that it will be useful,            
// but WITHOUT ANY WARRANTY; without even the implied warranty of             
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
// GNU General Public License (http://www.gnu.org/licenses/gpl.txt)           
// for more details.                                                          
//                                                                          
// --------------------------------------------------------------------------
/*

uniform float ambientRef;
uniform float diffuseRef;
uniform float specularRef;
uniform float shininess;

varying vec4 p;
varying vec3 n;

void main (void) {
    vec3 P = vec3 (gl_ModelViewMatrix * p); //Position du point à éclairer
    vec3 N = normalize (gl_NormalMatrix * n); //Normal en ce point
    vec3 V = normalize (-P); //Vecteur de vue

    for(int i = 0 ; i< 3 ; ++i) {
    
        vec4 Isa = gl_LightModel.ambient;
        vec4 Ka = gl_FrontMaterial.ambient;
        vec4 Ia = Isa * Ka;

        vec4 I = ambientRef * Ia;

   
        vec4 Isd = gl_LightSource[0].diffuse;
        vec4 Kd = gl_FrontMaterial.diffuse;
        vec3 vec_ps = vec3 (gl_LightSource[0].position.xyz - P);
        vec_ps = normalize(vec_ps);
        vec4 Id = Isd * Kd * dot(vec_ps, P);

        I += diffuseRef * Id;

        vec4 Iss = gl_LightSource[0].specular;
        vec4 Ks = gl_FrontMaterial.specular;

        vec3 R = vec3 ((2. * dot(vec_ps, P) * vec_ps) - P);
        R = normalize(R);

        vec4 Is = Iss * Ks * pow(dot(R, V), shininess);

        I += specularRef * Is;

    }

    ////////////////////////////////////////////////
    //Eclairage de Phong à calculer en utilisant
    ////////////////////////////////////////////////
    // gl_LightSource[i].position.xyz Position de la lumière i
    // gl_LightSource[i].diffuse Couleur diffuse de la lumière i
    // gl_LightSource[i].specular Couleur speculaire de la lumière i
    // gl_FrontMaterial.diffuse Matériaux diffus de l'objet
    // gl_FrontMaterial.specular Matériaux speculaire de l'objet


    gl_FragColor = vec4 (I.xyz, 1);
}
*/
/*
uniform float ambientRef;
uniform float diffuseRef;
uniform float specularRef;
uniform float shininess;
uniform int levels;

varying vec4 p;
varying vec3 n;


void main (void) {
    vec3 P = vec3 (gl_ModelViewMatrix * p);
    vec3 N = normalize (gl_NormalMatrix * n);
    vec3 V = normalize (-P);



    vec4 Isa = gl_LightModel.ambient;
    vec4 Ka = gl_FrontMaterial.ambient;
    vec4 Ia = Isa*Ka;
    vec4 I = ambientRef * Ia ;

    for (int i = 0; i < 1; i++) {

        vec4 Isd = gl_LightSource[i].diffuse;
        vec4 Kd = gl_FrontMaterial.diffuse;

        vec3 L = normalize (gl_LightSource[i].position.xyz - P);
        //Exercice 3
        //mettre à jour L pour avoir une lumière placée à la position de la caméra
        L = V
        //dotLN = 0;
        float dotLN = max(dot (L, N), 0.);

        //vec4 Id = Isd*Kd*dotLN;

        vec4 Iss = gl_LightSource[i].specular;
        vec4 Ks = gl_FrontMaterial.specular;

        vec3 R = reflect (-L, N);
        //ou
        // R = 2.*dotLN*N -L;

        float dotRV = max(dot(R, V), 0.0);
        vec4 Is = Iss*Ks*max (0.0, pow (dotRV, shininess));

        //TOON shading a completer

        //Caluler la valeur de la variable diffuse pour le cel shading en utilisant la variable levels
        float diffuse = 0.75;
        
        for(int j = 0; j < levels; j++){
            if(dotLN >=  float(j) / float(levels)  && dotLN <  ( float(j) / float(levels) + 1.0 / float(levels))){
                diffuse = float(j) / float(levels);
            }
        }
        if(dot (N,V) < 0.3){
            diffuse = 0.0;
        }
             
    
        vec4 Id = Isd*Kd*diffuse;


        //Caluler l'intensité en utilisant 'diffuse' pour moduler la couleur : indication utiliser Kd
        I += diffuseRef * Id + specularRef * Is;

        //Ajouter un bord noir en utilisant un seuil sur le produit scalaire entre N et V


    }

    gl_FragColor =vec4 (I.xyz, 1.);
}
*/
/*
uniform vec3 lightDir;
varying vec3 normal;

uniform float ambientRef;
uniform float diffuseRef;
uniform float specularRef;
uniform float shininess;
uniform int levels;

varying vec4 p;
varying vec3 n;

void main()
{
    float intensity;
    vec4 color;
    intensity = dot(lightDir,normalize(normal));

    if (intensity > 0.95)
        color = vec4(1.0,0.5,0.5,1.0);
    else if (intensity > 0.5)
        color = vec4(0.6,0.3,0.3,1.0);
    else if (intensity > 0.25)
        color = vec4(0.4,0.2,0.2,1.0);
    else
        color = vec4(0.2,0.1,0.1,1.0);
    gl_FragColor = color;

}
*/
// --------------------------------------------------------------------------
// gMini,
// a minimal Glut/OpenGL app to extend
//
// Copyright(C) 2007-2009
// Tamy Boubekeur
//
// All rights reserved.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License (http://www.gnu.org/licenses/gpl.txt)
// for more details.
//
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// gMini,
// a minimal Glut/OpenGL app to extend                              
//
// Copyright(C) 2007-2009                
// Tamy Boubekeur
//                                                                            
// All rights reserved.                                                       
//                                                                            
// This program is free software; you can redistribute it and/or modify       
// it under the terms of the GNU General Public License as published by       
// the Free Software Foundation; either version 2 of the License, or          
// (at your option) any later version.                                        
//                                                                            
// This program is distributed in the hope that it will be useful,            
// but WITHOUT ANY WARRANTY; without even the implied warranty of             
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
// GNU General Public License (http://www.gnu.org/licenses/gpl.txt)           
// for more details.                                                          
//                                                                          
// --------------------------------------------------------------------------
uniform float ambientRef;
uniform float diffuseRef;
uniform float specRef;
uniform float shininess;

varying vec4 p;
varying vec3 n;


void main (void) {
    vec3 P = vec3 (gl_ModelViewMatrix * p); //Position du point à éclairer
    vec3 N = normalize (gl_NormalMatrix * n); //Normal en ce point
    vec3 V = normalize (-P); //Vecteur de vue

    //float coeffBruit = 5.00;
    //vec3 fragment = coeffBruit * P;
    //vec3 monRandomVec = fbm3ToVec3(fragment);
    //N += monRandomVec;  // decommenter pour faire de bruit

    
    vec4 refAmb = ambientRef * gl_LightModel.ambient*gl_FrontMaterial.ambient ;
    vec4 lightContribution = refAmb;
    for(int i = 0; i < 3; i++){
        
        vec3 L = normalize(gl_LightSource[i].position.xyz - P); //Vecteur de la lumière
        vec3 R = 2.00*dot(N,L)*N - L; //Vecteur de réflexion

              
        float toonShading;

        if (max(dot(L,N),0.0) > 0.5)//pour cartoonish look
            toonShading = 1.0;
        else if (max(dot(L,N),0.0) > 0.3)
            toonShading = 0.5;
        else if ((max(dot(L,N),0.0) > 0.0))
            toonShading = 0.33;
        else
            toonShading = 0.1;

        vec4 refDiff = diffuseRef * gl_LightSource[i].diffuse * gl_FrontMaterial.diffuse * max(dot(L,N) ,0.0);
        lightContribution += refDiff;

        vec4 refSpec = specRef * gl_LightSource[i].specular * gl_FrontMaterial.specular * pow(max(dot(R,V),0.0),shininess);
        lightContribution += refSpec;
    
        lightContribution *= toonShading;
    }
    
    ////////////////////////////////////////////////
    //Eclairage de Phong à calculer en utilisant
    ///////////////////////////////////////////////
    // gl_LightSource[i].position.xyz Position de la lumière i
    // gl_LightSource[i].diffuse Couleur diffuse de la lumière i
    // gl_LightSource[i].specular Couleur speculaire de la lumière i
    // gl_FrontMaterial.diffuse Matériaux diffus de l'objet
    // gl_FrontMaterial.specular Matériaux speculaire de l'objet


    float rouge = lightContribution.x;
    float green = lightContribution.y;
    float blue = lightContribution.z;

    int nombreSeuillage = 10;

    for(int i = 0;i < nombreSeuillage;i++){    
        float seuille = float(i) / float(nombreSeuillage);
        if(lightContribution.x > seuille){
            rouge = seuille;
            //aChangeR = 1;
        }
        if(lightContribution.b > seuille){
            blue = seuille;
            //aChangeB = 1;
        }
        if(lightContribution.g > seuille){
            green = seuille;
            //aChangeG = 1;
        }
    }
    gl_FragColor =vec4(lightContribution.xyz,1.0);
    //gl_FragColor =vec4(rouge , green , blue , 1.0);
    
}
 

